
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";
window.rezultati1 = new Array(4);
window.rezultati2 = new Array(4);

function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}
function creatPatientsEHR() {
	var sessionId = getSessionId();
	var Fname = $("#kreirajIme").val();
	var Lname = $("#kreirajPriimek").val();
	var BirthDate = $("#kreirajDatumRojstva").val();
	$("#name").text(Fname+" "+Lname);
	var starost = getAge(BirthDate);
	$("#starost").text(starost);
	if (!Fname || !Lname || !BirthDate || Fname.trim().length == 0 ||
      Lname.trim().length == 0 || BirthDate.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: Fname,
		            lastNames: Lname,
		            dateOfBirth: BirthDate,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#dodatnoSporocilo").text("Uspešno kreiran EHR");
                            $("#dodajVitalnoEHR").text(ehrId);
		                    document.getElementById("dodatnoSporocilo").setAttribute("style","color:green")
		                }
		            },
		            error: function(err) {
		            	$("#dodatnoSporocilo").text("Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
                    	document.getElementById("kreirajSporocilo1").setAttribute("style","color:red")
		            }
		        });
		    }
		});
	}
}

function getAge(dateString) 
{
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) 
    {
        age--;
    }
    return age;
}

 function dodajMeritve() {
	var sessionId = getSessionId();

	var ehrId = $("#dodajVitalnoEHR").text();
	var DateAndTime = $("#dodajVitalnoDatumInUra").val();
	var Heigh = $("#dodajVitalnoTelesnaVisina").val();
	var Weight = $("#dodajVitalnoTelesnaTeza").val();
	var Temperature = $("#dodajVitalnoTelesnaTemperatura").val();
	var BloodPressureM = $("#dodajVitalnoNasicenostKrviSKisikom").val();

	$("#bmi").text(parseFloat(calculateBMI(Weight, Heigh)).toFixed(2));
	$("#SpO2").text(BloodPressureM);
	
	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": DateAndTime,
		    "vital_signs/height_length/any_event/body_height_length": Heigh,
		    "vital_signs/body_weight/any_event/body_weight": Weight,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": Temperature,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C"
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#kreirajSporocilo1").text(res.meta.href);
		        document.getElementById("kreirajSporocilo1").setAttribute("style","color:green");
		    },
		    error: function(err) {
		    	$("#kreirajSporocilo1").text(JSON.parse(err.responseText).userMessage + "'!");
		    	document.getElementById("kreirajSporocilo1").setAttribute("style","color:red")
		    }
		});
	}
	
}

function calculateBMI(weight, height){
	if(weight > 0 && height > 0){	
		var finalBmi = weight/(height/100*height/100);
	}
	return finalBmi;
}

function findRange(d, s){
	if(d<60 && s<90){
		var pressure = "low";
	}else if(d>=60 && d<=80 && s>=90 && s<=120){
		var pressure = "normal";
	}else if(d>80 && d<=90 && s>120 && s<=140){
		var pressure = "pre-high";
	}else 
		var pressure = "high";
		
	return pressure;
}

function generateData(nPatient) {
	var name1;
	var name2;
	var birth;
	var ehrId;
	if(nPatient == 1){
		name1 = "John";
		name2 = "Smith";
		birth = "2000-01-13";
	}else if(nPatient == 2){
		name1 = "Siddig";
		name2 = "El Faddil";
		birth = "1989-03-19";
	}else if(nPatient == 3){
		name1 = "Alexandra";
		name2 = "Rene";
		birth = "1957-02-28";
	}

	var sessionId = getSessionId();

	$.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	$.ajax({
	    url: baseUrl + "/ehr",
	    type: 'POST',
	    success: function (data) {
	        var ehrId = data.ehrId;
	        var partyData = {
	            firstNames: name1,
	            lastNames: name2,
	            dateOfBirth: birth,
	            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
	        };
	        $.ajax({
	            url: baseUrl + "/demographics/party",
	            type: 'POST',
	            contentType: 'application/json',
	            data: JSON.stringify(partyData),
	            success: function (party) {
	                if (party.action == 'CREATE') {
	                    if(nPatient == 1){
							$("#prviPacient").html(ehrId);
							AddVitalSigns("1",ehrId);
						}else if(nPatient == 2){
							$("#drugiPacient").html(ehrId);
							AddVitalSigns("2", ehrId);
						}else if(nPatient == 3){
							$("#tretiPacient").html(ehrId);
							AddVitalSigns("3", ehrId);
						}
						                }
	            },
	            error: function(err) {
	            }
	        });
	    }
	});
}

function generatePButton(){
	generateData("1");
	generateData("2");
	generateData("3");
}
function AddVitalSigns(PatientNumber, ehrId) {
	var sessionId = getSessionId();
	var data = new Array(4);
	var heignt = new Array(4);
	var weight = new Array(4);
	var temperature = new Array(4);
	
	if(PatientNumber=="1"){
			data[0] = "2014-5-13T10:10";
			data[1] = "2015-5-1T12:27";
			data[2] = "2016-10-28T23:30";
			data[3] = "2017-1-1T13:17";
			heignt[0] = 180;
			heignt[1] = 181;
			heignt[2] = 189;
			heignt[3] = 190;
			temperature[0] = 36;
			temperature[1] = 37.5;
			temperature[2] = 36,7;
			temperature[3] = 40;
			weight[0] = 100;
			weight[1] = 70;
			weight[2] = 120;
			weight[3] = 85;
	}if(PatientNumber=="2"){
			data[0] = "2014-5-13T10:10";
			data[1] = "2015-5-1T12:27";
			data[2] = "2016-10-28T23:30";
			data[3] = "2017-1-1T13:17";
			heignt[0] = 190;
			heignt[1] = 195;
			heignt[2] = 192;
			heignt[3] = 200;
			temperature[0] = 37;
			temperature[1] = 35;
			temperature[2] = 35,7;
			temperature[3] = 43;
			weight[0] = 130;
			weight[1] = 70;
			weight[2] = 100;
			weight[3] = 90;
}else if(PatientNumber=="3"){
			data[0] = "2014-5-13T10:10";
			data[1] = "2015-5-1T12:27";
			data[2] = "2016-10-28T23:30";
			data[3] = "2017-1-1T13:17";
			heignt[0] = 160;
			heignt[1] = 162;
			heignt[2] = 180;
			heignt[3] = 182;
			temperature[0] = 35;
			temperature[1] = 42;
			temperature[2] = 36;
			temperature[3] = 38,5;
			weight[0] = 60;
			weight[1] = 70;
			weight[2] = 55;
			weight[3] = 76;
}
	
	for(var i = 0; i < 4; i++) {
		 $.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		
		var data = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": data[i],
		    "vital_signs/height_length/any_event/body_height_length": heignt[i],
		    "vital_signs/body_weight/any_event/body_weight": weight[i],
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": temperature[i],
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		};
		var parameters = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: 'Someone'
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parameters),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(data),
		    async: false 
		});
	}	
	
}
